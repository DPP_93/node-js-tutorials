expressModule = {} || expressModule;

expressModule.express = require("express");
expressModule.app = expressModule.express();

//*******************BEGINNING

//expressModule.app.get("/", function(req, res){
//    console.log(req);
//    console.log(res);
//    res.send("DAS IST KEINE HALLO WELT");
//});
//
//expressModule.server = expressModule.app.listen(8081, function(){
//    
//    var host = expressModule.server.address().address;
//    var port = expressModule.server.address().port;
//    
//    console.log("Przykładowa apka nasłuchuje na http://%s:%s", host, port);
//    
//});


//*******************BASIC ROUTING

//Respons wuth "DAS IST KEINE HALLO WELT" on the homepage
//expressModule.app.get("/", function(req, res){
//    console.log("Got a GET request for homepage");
//    res.send("DAS IST KEINE HALLO WELT GET");
//});
//
////This respond with POST request
//expressModule.app.post("/", function(req, res){
//    console.log("Got w POST");
//    res.send("DAS IST KEINE HALLO WELT POST");
//});
//
////This respond with delete request for the /del_user page
//expressModule.app.delete("/del_user", function(req, res){
//    console.log("Got DELETE for /del_user");
//    res.send("DAS IST KEINE HALLO WELT DELETE");
//});
//
//// This responds a GET request for the /list_user page.
//expressModule.app.get('/list_user', function (req, res) {
//    console.log("Got a GET request for /list_user");
//    res.send('Page Listing');
//});
//
//// This responds a GET request for abcd, abxcd, ab123cd, and so on
//expressModule.app.get("/ab*cd", function(req, res){
//    res.send("MAGIC SEND");
//});
//
//expressModule.server = expressModule.app.listen(8081, function(){
//    var host = expressModule.server.address().address;
//    var port = expressModule.server.address().port;
//    
//    console.log("Exam ple app listening at http://%s:%s", host, port);
//});


//***************************STATIC FILES
//expressModule.app.use(expressModule.express.static("public"));
//
//expressModule.app.get("/", function(req, res){
//    res.send("DAS IST KEINE HALE WELT");
//});
//
//expressModule.server = expressModule.app.listen(8081, function(){
//    var host = expressModule.server.address().address;
//    var port = expressModule.server.address().address;
//    
//    console.log("Example app listening at http://%s:%s", host, port)
//});


//***************************GET Method
//This is connected with index.html
//expressModule.app.use(expressModule.express.static("public"));
//expressModule.app.get("/index.html", function(req, res){
//    //__dirname means actual diractory
//    res.sendFile(__dirname + "/index.html");
//});
//
//expressModule.app.get("/process_get", function(req, res){
//    //Prepare output in JSON format
//    var response = { first_name:req.query.first_name,
//                   last_name:req.query.last_name
//                   };
//    console.log(response);
//    res.end(JSON.stringify(response));
//});
//
//expressModule.server = expressModule.app.listen(8081, function(){
//    var host = expressModule.server.address().address;
//    var port = expressModule.server.address().port;
//    
//    console.log("Example app listening at http://%s:%s", host, port);
//});

//***************************POST METHOD
//expressModule.bodyParsesr = require("body-parser");
//
//expressModule.urlEncodedParser = expressModule.bodyParsesr.urlencoded({extended: false});
//
//expressModule.app.use(expressModule.express.static("public"));
//expressModule.app.get("/index.html", function(req, res){
//    //__dirname means actual diractory
//    res.sendFile(__dirname + "/index.html");
//});
//
//expressModule.app.post("/process_post", expressModule.urlEncodedParser, function(req,res){
//    //Prepare output in json format
//    var response = { first_name:req.body.first_name,
//                   last_name:req.body.last_name
//                   };
//    console.log(response);
//    res.end(JSON.stringify(response));
//});
//
//expressModule.server = expressModule.app.listen(8081, function(){
//    var host = expressModule.server.address().address;
//    var port = expressModule.server.address().port;
//    
//    console.log("Example app listening at http://%s:%s", host, port);
//});

//*****************************FILE UPLOAD //CHUJ WIE CZEMU NIE DZIAŁA
//expressModule.fs = require("fs");
//expressModule.bodyParser = require("body-parser");
//expressModule.multer = require("multer");//allow to multipart/form-data operations
//
//expressModule.app.use(expressModule.express.static("public"));
//expressModule.app.use(expressModule.bodyParser.urlencoded({extended: false}));
//expressModule.app.use(expressModule.multer({dest: "tmp/"}));
//
//
//expressModule.app.get("/fileUp.html", function(req, res){
//    //__dirname means actual diractory
//    res.sendFile(__dirname + "/fileUp.html");
//});
//
//expressModule.app.post("/file_upload", function(req, res){
//    console.log(req.files.file.name);
//    console.log(req.files.file.path);
//    console.log(req.files.file.type);
//    var response;
//    var file = __dirname + "/" + req.files.file.name;
//    expressModule.fs.readFile(req.files.file.path, function(err, data){
//        if(err){
//            console.log(err);
//        }else{
//            response = {
//                message: "File upload successfully",
//                filename: req.files.file.name
//            };
//        }
//        console.log(response);
//        res.end(JSON.stringify(response));
//    });
//});
//
//expressModule.server = expressModule.app.listen(8081, function(){
//    var host = expressModule.server.address().address;
//    var port = expressModule.server.address().port;
//    
//    console.log("Example app listening at http://%s:%s", host, port);
//});

//*******************************COOKIES MANAGMENT
expressModule.cookieParser = require("cookie-parser");

expressModule.app.use(expressModule.cookieParser());
expressModule.app.get("/", function(req, res){
    console.log("Cookies ", req.cookies);
});

expressModule.app.listen(8081);