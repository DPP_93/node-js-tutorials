clientModule = {} || clientModule;

clientModule.http = require("http");

//Options to be used by request
clientModule.options = {
    host: "localhost",
    port: "8081",
    path: "/index.htm"
};

//Callback function used to deal with response
clientModule.callback = function(response){
    //Continuously update stream with data
    var body = "";
    response.on("data", function(data){
        body += data;
    });
    
    response.on("end", function(){
        //So when all will be send
        console.log(body);
    });
    
    response.on("error", function(err){
        console.error(err);
    });
};

//Make server request
clientModule.req = clientModule.http.request(clientModule.options, clientModule.callback);
clientModule.req.end();