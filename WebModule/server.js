serverModule = {} || serverModule;

serverModule.http = require("http");
serverModule.fs = require("fs");
serverModule.url = require("url");

//Create Server
serverModule.http.createServer(function(request, response){
    //Parse request
    var pathname = serverModule.url.parse(request.url).pathname;
    console.log("Request for "+pathname+" received.");
    
    //Read requested file
    serverModule.fs.readFile(pathname.substr(1), function(err, data){
        if(err){
            console.error(err);
            response.writeHead(404, {"Content-Type":"text/html"});
        }else{
            //Page found
            response.writeHead(200, {"Content-Type":"text/html"});
            response.write(data.toString());
        }
        response.end();
    });
    
}).listen(8081);