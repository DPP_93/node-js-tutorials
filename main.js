//We use require directive to load http module and store returned HTTP instance into http variable as follows
TutorialNode = {} || TutorialNode;
//TutorialNode.http = require("http");
//TutorialNode.fs = require("fs");
//

/*
TutorialNode.http.createServer(function(request, response){
	// Send the HTTP header 
	// HTTP Status: 200 : OK
	// Content Type: text/plain
	response.writeHead(200, {"Content-type": "text/plain"});
	
	// Send the response body as "Hello World"
	response.end("HelloĹ‚ World");
	
}).listen(8081);//Listen on port 8081*/
//// Console will print the message
//console.log('Server running at http://127.0.0.1:8081/');


//******************************CALLBACKS

//var data = TutorialNode.fs.readFileSync("input.txt")
//console.log(data.toString());

//TutorialNode.fs.readFile("input.txt", function(err, data){
//	if(err){
//		return console.error(err);
//	}
//	console.log(data.toString());
//});
//


//*******************************EVENTS
////Import event module, which allow to use events
//TutorialNode.events = require('events');
////Event Emmiter allow fire events
//TutorialNode.eventEmmiter = new TutorialNode.events.EventEmitter();
//
////Create event handler
//TutorialNode.eventHandler = function connected(){
//    console.log("Connection successfull");
//    //Fire event
//    TutorialNode.eventEmmiter.emit("data_received");
//};
//
////Bind event name with handler
//TutorialNode.eventEmmiter.on("connection", TutorialNode.eventHandler);
//
////Bind data_received event with anonymous function
//TutorialNode.eventEmmiter.on("data_received", function(){
//   console.log("DATA RECEIVED"); 
//});
//
//TutorialNode.eventEmmiter.emit("connection");

//**********************************BUFFERS

//First method of creating buffers
//Buffer is a global class and is accessed without importing module
//TutorialNode.buffer1 = new Buffer(10); //Create buffer of 10 octets
//
////Second buffer
////Create new buffer using array of octets
//TutorialNode.buffer2 = new Buffer([10,20,30,40,50]);
//
////Third method
////Create a buffer with string and encoding, default - utf-8
//TutorialNode.buffer3 = new Buffer("HELLOŁ WORLD", "utf-8");
//
//TutorialNode.buffer = new Buffer(256);
//TutorialNode.len = TutorialNode.buffer.write("Helloł world MOTHAFUCKA");
//console.log("Octets written "+TutorialNode.len + " from "+TutorialNode.buffer.length);
//
//TutorialNode.readBuffer = new Buffer(26);
//var i = 0;
//for(i; i<TutorialNode.readBuffer.length; i++){
//    TutorialNode.readBuffer[i] = i + 97; //97 start 'a' in ASCII
//}
//
////To read buffer we use toString
//console.log(TutorialNode.readBuffer.toString("ascii"));
//console.log(TutorialNode.readBuffer.toString("ascii", 0, 5));
//console.log(TutorialNode.readBuffer.toString("utf-8"));
//console.log(TutorialNode.readBuffer.toString("utf-8",0, 5));
//console.log(TutorialNode.readBuffer.toString(undefined,0, 5));
//
////Convert buffer to JSON
//console.log(TutorialNode.readBuffer.toJSON(TutorialNode.readBuffer));

//****************************************STREAMS

//TutorialNode.fs = require("fs");
//TutorialNode.data = '';
//
////Reading from stream
//
////Create a readable stream
//TutorialNode.readableStream = TutorialNode.fs.createReadStream("input.txt");
////Set encoding to UTF-8
//TutorialNode.readableStream.setEncoding("utf8");
//
////Handle events
////data - fired when it's sth to read
////end - when it's no more things to read
////error - when there's any error in receiving or writing data
////finish - fired when all data has been flushed
//
//TutorialNode.readableStream.on("data", function(chunk){
//   TutorialNode.data += chunk; 
//});
//
//TutorialNode.readableStream.on("end", function(){
//    console.log("No more data to read");
//    console.log(TutorialNode.data);
//});
//
//TutorialNode.readableStream.on("error", function(err){
//   console.log(err.stack); 
//});
//console.log("END END END");
//
//
////Writing to stream
////Create stream
//TutorialNode.writerStream = TutorialNode.fs.createWriteStream("output.txt");
//TutorialNode.writeData = "I LIKE TRAINS!!!";
//
////Write data with encoding utf8
//TutorialNode.writerStream.write(TutorialNode.writeData, "utf8");
////Mark end of file
//TutorialNode.writerStream.end();
//
//TutorialNode.writerStream.on("finish", function(){
//   console.log("writing data completed"); 
//});
//
//TutorialNode.writerStream.on("error", function(err){
//    console.log(err.stack);
//});
//
//
////Piping streams
//TutorialNode.readableStream = TutorialNode.fs.createReadStream("input.txt");
//TutorialNode.writerStream = TutorialNode.fs.createWriteStream("output.txt");
//TutorialNode.readableStream.pipe(TutorialNode.writerStream);
//TutorialNode.writerStream.end();
//console.log("piping complete, now you have train in ascii in output.txt");
//
//
////Chainning streams
//TutorialNode.gzip = require("zlib");
//TutorialNode.fs.createReadStream("output.txt").pipe(TutorialNode.gzip.createGzip()).pipe(TutorialNode.fs.createWriteStream("output.txt.gz"));
//console.log("File compressed");
//
////Now I'll decompress this file
//TutorialNode.fs.createReadStream("output.txt.gz").pipe(TutorialNode.gzip.createGunzip()).pipe(TutorialNode.fs.createWriteStream("inputFromGZ.txt"));

//************************************************FILE SYSTEM

TutorialNode.fs = require("fs");

////Asynchronous read
//TutorialNode.fs.readFile("input.txt", function(err, data) {
//   if(err){
//       return console.error(err);
//   }
//   console.log("Asynchronous read" +data.toString());
//});
//
////Synchronous read 
//TutorialNode.synchroData = TutorialNode.fs.readFileSync("input.txt");
//console.log(TutorialNode.synchroData.toString());
//console.log("END END END");
//
//
////Asynchronous opening file
//TutorialNode.fs.open("input.txt", "r+", function(err, fd){
//    if(err){
//        console.error(err);
//    }
//    console.log("Opening successfull");
//});


//console.log("Going to get file info");
//TutorialNode.fs.stat("input.txt", function(err, stats){
//    if(err){
//        console.error(err);
//    }
//    console.log(stats);
//    console.log("Get file info successfully");
//    
//    console.log("isFile? " + stats.isFile());
//    console.log("isDirectory? " +stats.isDirectory());
//});

TutorialNode.fs.writeFile("output.txt", "KA-ME-HA-ME-HA", function(err){
    if(err){
        console.error(err);
    }
    
    console.log("Data write sucessfull");
    console.log("Let's read it :D");
    TutorialNode.fs.readFile("output.txt", function(err, data){
        if(err){
            console.error(err);
        }
        console.log("File read successfull "+ data.toString());
    });
    
});

console.log("END END END");


